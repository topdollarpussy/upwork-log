// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
(function(){
    var cache = {};
    
    this.tmpl = function tmpl(str, data){
        // Figure out if we're getting a template, or if we need to
        // load the template - and be sure to cache the result.
        var fn = !/\W/.test(str) ?
				cache[str] = cache[str] ||
				tmpl(document.getElementById(str).innerHTML) :
				
				// Generate a reusable function that will serve as a template
				// generator (and which will be cached).
				new Function("obj",
							 "var p=[],print=function(){p.push.apply(p,arguments);};" +
							 
							 // Introduce the data as local variables using with(){}
							 "with(obj){p.push('" +
							 
							 // Convert the template into pure JavaScript
							 str
							 .replace(/[\r\t\n]/g, " ")
							 .split("<%").join("\t")
							 .replace(/((^|%>)[^\t]*)'/g, "$1\r")
							 .replace(/\t=(.*?)%>/g, "',$1,'")
							 .split("\t").join("');")
							 .split("%>").join("p.push('")
							 .split("\r").join("\\'")
							 + "');}return p.join('');");
		
        // Provide some basic currying to the user
        return data ? fn( data ) : fn;
    };
})();

function nameColor(name) {
	var hash = 0;
	for (var i = 0; i < name.length; i++) {
		hash = name.charCodeAt(i) + ((hash << 5) - hash);
		hash &= hash;
	}
	return 'hsl(' + (hash % 360) + ', 25%, 50%)';
}

(function () {
	var $ = document.querySelector.bind(document);
	var $$ = document.querySelectorAll.bind(document);

	var messageTemplate;
	var pageState = { channel: null, date: null };

	function get(url, success, error) {
		var req = new XMLHttpRequest();
		req.responseType = 'json';
		req.addEventListener('load', function () {
			if (success !== undefined) {
				success(this.response);
			}
		});
		req.open('GET', url);
		req.send();
	}

	function renderMessages(messages) {
		var messagesHtml = messages.map(messageTemplate).join('');
		$('.messages').innerHTML = messagesHtml;
		$('.no-messages').className = (messages.length > 0 ? 'hidden ' : '') + 'no-messages';
	}

	/**
	 * Updates the message list using provided params
	 * @param {String} channel
	 * @param {Date} date
	 */
	function updateMessages(channel, date) {
		var params = [];

		if (channel)
			params.push('channel=' + channel);
		if (date)
			params.push('day=' + date.toISOString());

		get('/api/messages' + (params.length > 0 ? '?' + params.join('&') : ''), function success(response) {
			renderMessages(response.messages);
		});
	}

	function updateChannelsList() {
		get('/api/channels', function success(response) {
			var channels = $('#channels');
			response.channels.map(function (chan) {
				var option = document.createElement('option');
				option.value = chan;
				option.innerText = '#' + chan;
				return option;
			}).forEach(function (option) {
				channels.appendChild(option);
			});
			updateTopNav(pageState);
		});
	}

	function updateHistoryNav(pageState) {
		var prevDay = { channel: pageState.channel, date: moment(pageState.date).subtract(1, 'days') };
		$('.history-nav > .prev > a').setAttribute('href', pageUrlFor(prevDay));

		var isToday = moment().diff(pageState.date, 'days') == 0;
		$('.history-nav > .next').className = 'next' + (isToday ? ' hidden' : '');

		if (!isToday) {
			var nextDay = { channel: pageState.channel, date: moment(pageState.date).add(1, 'days') };
			$('.history-nav > .next > a').setAttribute('href', pageUrlFor(nextDay));
		}
	}

	function updateTopNav(pageState) {
		$('#channels').value = pageState.channel;
		$('#date').value = moment(pageState.date).format('YYYY-MM-DD');
	}

	/**
	 * Updates the message list using the current page state.
	 */
	function updatePage(pageState) {
		updateMessages(pageState.channel, pageState.date);
		updateHistoryNav(pageState);
		updateTopNav(pageState);
	}

	/**
	 * Generates URL for a certain page state
	 */
	function pageUrlFor(pageState) {
		var date = moment(pageState.date).format('YYYY-MM-DD');
		return '/' + pageState.channel + '/' + date;
	}

	function updateStateFromNav() {
		pageState.channel = $('#channels').value;
		pageState.date = new Date($('#date').value);
		history.pushState({}, '', pageUrlFor(pageState));
		updatePage(pageState);
	}

	function updateDateFromNav(ev) {
		if (moment(ev.target.value).diff(pageState.date, 'days') != 0) {
			updateStateFromNav();
		}
	}

	function addSomeSinglePagePower(ev) {
		var href = ev.target.getAttribute('href');
		if (href != null) {
			history.pushState({}, '', href);
			route();
		}
		ev.preventDefault();
	}

	function route() {
		var url = location.pathname;
		var m;

		if (url == '/') {
			// Default page
			pageState.channel = 'general';
			pageState.date = new Date();
			updatePage(pageState);
		} else if ((m = url.match(/^\/(\w+)\/(\d{4}-\d{1,2}-\d{1,2})/))) {
			// Show date
			pageState.channel = m[1];
			pageState.date = new Date(m[2]);
			updatePage(pageState);
		}
	}

	document.addEventListener('DOMContentLoaded', function () {
		messageTemplate = tmpl(document.getElementById('message-template').innerHTML);

		updateChannelsList();
		route();

		// Events
		$('#channels').addEventListener('change', updateStateFromNav, false);
		$('#date').addEventListener('keyup', updateDateFromNav, false);
		$('#date').addEventListener('click', updateDateFromNav, false);

		// History API
		Array.prototype.forEach.call($$('.history-nav, header > nav'), function (e) {
			e.addEventListener('click', addSomeSinglePagePower, false);
		});
		window.addEventListener('popstate', route, false);
	}, false);
})();
