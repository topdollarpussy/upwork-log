import os

SETTINGS = {
    'APP_URL': 'http://upwork-log.herokuapp.com/',
    'MONGO_URI': os.environ['MONGO_URI'],
    'MONGO_DB': os.environ['MONGO_DB'],
    'SLACK_TOKEN': os.environ['SLACK_TOKEN'],
    'SLACK_CLIENT_ID': '4550301824.7953826738',
    'SLACK_CLIENT_SECRET': os.environ['SLACK_CLIENT_SECRET'],
    'USERS_CACHE_TIME': 60 * 60 * 24,
    'CHANNELS_CACHE_TIME': 60 * 60 * 24 * 7,
    'EMOJI_CACHE_TIME': 60 * 60 * 24 * 14,
    'DEFAULT_CHANNEL': 'general',
    'TIMEZONE': 'Europe/Moscow'
}
