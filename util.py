import os
from urllib.parse import urlparse

from werkzeug.contrib.cache import SimpleCache, RedisCache

redis_url = os.getenv('REDISTOGO_URL', None)

if redis_url is None:
    cache = SimpleCache()
else:
    redis = urlparse(redis_url)
    cache = RedisCache(host=redis.hostname, port=redis.port,
                       password=redis.password)

def cached(cache_timeout):
    def wrapper(func):
        return Cache(func, cache_timeout)
    return wrapper

class Cache(object):
    def __init__(self, function, timeout):
        self._fn = function
        self._cache_key = function.__name__
        self._timeout = timeout

    def __call__(self, *args, **kwargs):
        value = cache.get(self._cache_key)
        if value is None:
            value = self.invalidated(*args, **kwargs)
        return value

    # Bypasses the cache and immediately calls the wrapped function
    def invalidated(self, *args, **kwargs):
        value = self._fn(*args, **kwargs)
        cache.set(self._cache_key, value, self._timeout)
        return value
