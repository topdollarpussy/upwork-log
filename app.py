import os, sys, re, datetime
import logging

from flask import Flask, render_template, jsonify, request, session, url_for, redirect
from flask_oauthlib.client import OAuth
from pymongo import MongoClient
from slacker import Slacker
import pytz
import markdown
import emoji

from settings import SETTINGS
from util import cached

timezone = pytz.timezone(SETTINGS['TIMEZONE'])

app = Flask(__name__)
app.secret_key = SETTINGS['SLACK_CLIENT_SECRET']
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.ERROR)

db = MongoClient(SETTINGS['MONGO_URI'], tz_aware=True)[SETTINGS['MONGO_DB']]
slack = Slacker(SETTINGS['SLACK_TOKEN'])

# Store sessions in Redis on production
redis_url = os.getenv('REDISTOGO_URL', None)

if redis_url is not None:
    import redis
    from redis_sessions import RedisSessionInterface

    redis = redis.from_url(redis_url)
    app.session_interface = RedisSessionInterface(redis)

# OAuth
oauth = OAuth().remote_app('Slack',
                           authorize_url='https://slack.com/oauth/authorize',
                           access_token_method='POST',
                           access_token_url='https://slack.com/api/oauth.access',
                           consumer_key=SETTINGS['SLACK_CLIENT_ID'],
                           consumer_secret=SETTINGS['SLACK_CLIENT_SECRET'],
                           request_token_params={'scope': 'identify'})

@oauth.tokengetter
def get_slack_token(token=None):
    return session.get('slack_token')

@cached(SETTINGS['EMOJI_CACHE_TIME'])
def get_emojis():
    response = slack.emoji.list()
    emojis = response.body['emoji']
    def check_alias(k, v):
        return emojis[v[6:]] if (v[0:6] == 'alias:' and v[6:] in emojis) else v          
    return { k: check_alias(k, v) for k, v in emojis.items() }

@cached(SETTINGS['USERS_CACHE_TIME'])
def get_users():
    response = slack.users.list()
    return { member['id']: member for member in response.body['members'] }

@cached(SETTINGS['CHANNELS_CACHE_TIME'])
def get_channels():
    response = slack.channels.list(exclude_archived=1)
    return { chan['id']: chan for chan in response.body['channels'] }

# Returns a channel name by its ID
def get_channel_name(channel_id):
    chans = get_channels()
    if channel_id not in chans:
        chans = get_channels.invalidated()
    if channel_id not in chans:
        return channel_id
    return chans[channel_id]['name']

# Returns a channel ID by its name
def get_channel_id(channel_name, invalidated=False):
    if invalidated:
        chans = get_channels.invalidated()
    else:
        chans = get_channels()

    for (chan_id, chan) in chans.items():
        if chan['name'] == channel_name:
            return chan_id

    if not invalidated:
        return get_channel_id(channel_name, True)
    else:
        return None

@app.route('/login/authorized')
def oauth_authorized():
    resp = oauth.authorized_response()
    if resp is None:
        return 'Access denied'
    next_url = request.args.get('next') or url_for('show_index')
    session['slack_token'] = (resp['access_token'], '')
    return redirect(next_url)

@app.route('/login')
def login():
    return oauth.authorize(callback=SETTINGS['APP_URL'] + url_for('oauth_authorized'),
                           next=request.referrer or None)

@app.route('/')
@app.route('/<path:path>')
def show_index(path=None):
    if 'slack_token' not in session:
        return redirect(url_for('login'))
    return render_template('index.html')

@app.route('/api/best')
def list_best_quotes():
    quotes = db.wisdom.find()
    return jsonify(quotes)

# Query params:
# ?channel=<channel_id>
# ?day=<timestamp>
@app.route('/api/messages')
def list_messages():
    if ('channel' not in request.args):
        chan_id = get_channel_id(SETTINGS['DEFAULT_CHANNEL'])
    else:
        chan_id = get_channel_id(request.args['channel'])

    if ('day' not in request.args):
        day = datetime.datetime.today()
    else:
        day = datetime.datetime.strptime(request.args['day'], '%Y-%m-%dT%H:%M:%S.%fZ')

    raw_messages = db.messages.find({
        'channel': chan_id,
        'timestamp': {
            '$gte': timezone.localize(datetime.datetime.combine(day.date(), datetime.time(0, 0, 0))),
            '$lte': timezone.localize(datetime.datetime.combine(day.date(), datetime.time(23, 59, 59, 999)))
        }
    })

    messages = []

    users = get_users()
    emojis = get_emojis()

    for msg in raw_messages:
        if msg['user'] not in users:
            users = get_users.invalidated()

        # Replace @mentions
        message = re.sub(r'<@(\w+?)(\|.+?)?>', lambda u: '<span class="mention">@' + users[u.group(1)]['name'] + '</span>', msg['message'])
        # Replace #channels
        message = re.sub(r'<#([A-Z0-9]+?)>', lambda c: '<span class="channel">#' + get_channel_name(c.group(1)) + '</span>', message)
        # Markdownify
        message = markdown.markdown(message, extensions=['markdown.extensions.nl2br'])
        # Replace emojis & custom emojis
        message = emoji.emojize(message)
        message = re.sub(r':(\w+):', lambda emo: '<img src="' + emojis[emo.group(1)] + '" class="emj" />' if emo.group(1) in emojis else emo.group(1), message) 

        author = users[msg['user']]['name'] if msg['user'] != 'USLACKBOT' else 'slackbot'

        messages.append({
            'id': str(msg['_id']),
            'author': author,
            'time': msg['timestamp'],
            'message': message
        })

    return jsonify({ 'ok': True, 'messages': messages })

@app.route('/api/channels')
def list_channels():
    return jsonify({
        'ok': True,
        'channels': [chan['name'] for chan in get_channels().values() if chan['is_member'] ]
    })

@app.route('/api/best')
def add_best():
    return []

@app.route('/api/best/<id>/vote')
def vote_best():
    return []

if __name__ == '__main__':
    app.run()
